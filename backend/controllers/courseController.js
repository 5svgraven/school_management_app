const Course = require("../models/Course");
//Mamage Course
// Start of course
//TODO Create Course
module.exports.createCourse = async (params) => {
  let newCourse = new Course({
    name: params.name,
    maxStudent: params.maxStudent,
    createdBy: params.createdBy,
  });
  let result = await newCourse.save();
  return result ? true : false;
};

//TODO Add Subjects to Course
module.exports.addSubjectToCourse = async (params) => {
  const courseResult = await Course.findById(courseId);
  courseResult.subjects.push({
    subjectId: params.subjectId,
  });
  const result = await courseResult.save();
  return result ? true : false;
};

//TODO Get All Course
module.exports.getAllCourses = async () => {
  const result = await Course.find({});
  return result;
};

//TODO Single Course
module.exports.getOneCourse = async (params) => {
  const result = await Course.findById(params.courseId);
  return result;
};

//TODO Update Course
module.exports.updateCourse = async (params) => {
  let updateCourse = {
    name: params.name,
    maxStudent: params.maxStudent,
  };
  let result = await Course.findByIdAndUpdate(params.courseId, updateCourse);
  return result ? true : false;
};

//TODO Delete Course
module.exports.deleteCourse = async (params) => {
  const result = await Course.findByIdAndDelete(params.courseId);
  return result ? true : false;
};
//TODO enroll student to a Course
module.exports.enrollStudentToCourse = async (params) => {
  const course = await Course.findById(params.courseId);
  course.students.push({
    studentId: params.studentId,
  });
  let updateEnrollment = {
    courseId: params.courseId,
  };
  const resultStudent = await Student.findByIdAndUpdate(
    params.studentId,
    updateEnrollment
  );
  const result = await course.save();
  return result ? (resultStudent ? true : false) : false;
};
