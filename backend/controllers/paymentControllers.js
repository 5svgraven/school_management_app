const Payment = require("../models/Payment");

//start of Payments
//TODO Get All Payment
module.exports.getAllPayment = async () => {
  const result = await Payment.find({});
  return result;
};
//TODO Single Payment
module.exports.getOnePayment = async (params) => {
  const result = await Payment.findById(params.paymentId);
  return result;
};
//TODO Update Payment
module.exports.updatePayment = async (params) => {
  let updatePayment = {
    remarks: params.remarks,
    status: params.status,
  };
  let result = await Payment.findByIdAndUpdate(params.paymentId, updatePayment);
  return result ? true : false;
};
//TODO Delete Payment
module.exports.deletePayment = async (params) => {
  const result = await Payment.findByIdAndDelete(params.paymentId);
  return result ? true : false;
};
//end of Payments
