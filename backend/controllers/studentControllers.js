const Student = require("../models/Student");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.createStudent = async (params) => {
  let newStudent = new Student({
    firstname: params.firstname,
    lastname: params.lastname,
    age: params.age,
    gender: params.gender,
    birthday: params.birthday,
    addresss: params.addresss,
    mobileNo: params.mobileNo,
    course: params.course,
    year: params.year,
    parentsName: params.parentsName,
    parentsMobileNo: params.parentsMobileNo,
    username: params.username,
    password: bcrypt.hashSync(params.password, 15),
  });
  let resultStudent = await newStudent.save();

  // let newPayment = new Payment({
  //   name: "New Enrollment",
  //   amount: params.amount,
  //   payor: resultStudent.id,
  //   createdBy: resultStudent.id,
  // });
  // const resultPayment = await newPayment.save();

  // let newAdmission = new Admission({
  //   studentId: resultStudent.id,
  //   paymentId: resultPayment.id,
  // });

  const result = await newStudent.save();

  return result ? true : false;
};

//TODO Details
module.exports.detail = async (params) => {
  const result = await Student.findById(params.studentId);
  result.password = undefined;
  return result;
};

//TODO login
module.exports.login = async (params) => {
  const result = await Student.findOne({ username: params.username });
  if (result == null) {
    return false;
  }
  const isPasswordMatched = bcrypt.compareSync(
    params.password,
    result.password
  );
  if (isPasswordMatched) {
    return { access: auth.createAccessToken(result) };
  } else {
    return false;
  }
};

//TODO Get All Student
module.exports.getAllStudent = async () => {
  const result = await Student.find({});
  result.password = undefined;
  return result;
};
//TODO Single Student
module.exports.getOneStudent = async (params) => {
  const result = await Student.findById(params.studentId);
  result.password = undefined;
  return result;
};
//TODO Update Student
module.exports.updateStudent = async (params) => {
  let updateStudent = {
    firstname: params.firstname,
    lastname: params.lastname,
    age: params.age,
    gender: params.gender,
    birthday: params.birthday,
    addresss: params.addresss,
    mobileNo: params.mobileNo,
  };
  let result = await Student.findByIdAndUpdate(params.studentId, updateStudent);
  return result ? true : false;
};
//TODO Delete Student
module.exports.deleteStudent = async (params) => {
  const result = await Student.findByIdAndDelete(params.studentId);
  return result ? true : false;
};
