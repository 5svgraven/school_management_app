const Grades = require("../models/Grades");
//Manage Grades

//TODO Create Grades
module.exports.createGrades = async (params) => {
  let newGrade = new Grades({
    grade: params.grade,
    typeOfStudy: params.typeOfStudy,
    referenceId: params.referenceId,
    subjectId: params.subjectId,
    studentId: params.studentId,
    createdBy: params.teacherId,
  });
  let result = await newGrade.save();
  return result ? true : false;
};

//TODO Get All Grades
module.exports.getAllGrades = async () => {
  const result = await Grades.find({});
  return result;
};

//TODO Single Grade
module.exports.getOneModule = async (params) => {
  const result = await Grades.findById(params.gradeId);
  return result;
};

module.exports.getAllGradesByStudent = async (params) => {
  const result = await Grades.find({ studentId: params.studentId });
  return result;
};

//TODO Update Grade
module.exports.updateGrade = async (params) => {
  let updatedGrade = {
    grade: params.grade,
    typeOfStudy: params.typeOfStudy,
    referenceId: params.referenceId,
    subjectId: params.subjectId,
    studentId: params.studentId,
  };
  let result = await Grades.findByIdAndUpdate(params.gradeId, updatedGrade);
  return result ? true : false;
};

//TODO Delete Module
module.exports.deleteGrade = async (params) => {
  const result = await Grades.findByIdAndDelete(params.gradeId);
  return result ? true : false;
};
