const Subject = require("../models/Subject");

//TODO Create Subject
module.exports.createSubject = async (params) => {
  let newSubject = new Subject({
    name: params.name,
    subjectType: params.subjectType,
    teacer: params.teacer,
    subjectStart: params.subjectStart,
    subjectEnd: params.subjectEnd,
    room: params.room,
    maxStudent: params.maxStudent,
    createdBy: params.createdBy,
  });
  let result = await newSubject.save();
  return result ? true : false;
};

//TODO Get All Subject
module.exports.getAllSubjects = async () => {
  const result = await Subject.find({});
  return result;
};
//TODO Single Subject
module.exports.getOneSubject = async (params) => {
  const result = await Subject.findById(params.adminId);
  return result;
};
//TODO Update Subject
module.exports.updateSubject = async (params) => {
  let updateSubject = {
    name: params.name,
    subjectType: params.subjectType,
    teacer: params.teacer,
    subjectStart: params.subjectStart,
    subjectEnd: params.subjectEnd,
    room: params.room,
    maxStudent: params.maxStudent,
  };
  let result = await Subject.findByIdAndUpdate(params.subjectId, updateSubject);
  return result ? true : false;
};
//TODO Delete Subject
module.exports.deleteSubject = async (params) => {
  const result = await Subject.findByIdAndDelete(params.subjectId);
  return result ? true : false;
};

//TODO assign a subject to a class or course
module.exports.addSubjectToClass = async (params) => {
  const section = await Section.findById(paras.sectionId);
  section.subjects.push({
    subjectId: params.subjectId,
  });
  const result = await section.save();
  return result ? true : false;
};

module.exports.addSubjectToCourse = async (params) => {
  const course = await Course.findById(paras.courseId);
  course.subjects.push({
    subjectId: params.subjectId,
  });
  const result = await course.save();
  return result ? true : false;
};

//TODO add student to class or course
module.exports.addStudentToClass = async (params) => {
  const section = await Section.findById(paras.sectionId);
  section.students.push({
    studentId: params.studentId,
  });
  const result = await section.save();
  return result ? true : false;
};

module.exports.addStudentToCourse = async (params) => {
  const course = await Course.findById(paras.courseId);
  course.students.push({
    studentId: params.studentId,
  });
  const result = await course.save();
  return result ? true : false;
};
