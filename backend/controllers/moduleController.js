const Module = require("../models/Module");
//Mamage Course
// Start of course
//TODO Create Module
module.exports.createModule = async (params) => {
  let newModule = new Module({
    name: params.name,
    description: params.description,
    deadlineDate: params.deadlineDate,
    typeOfStudy: params.typeOfStudy,
    referenceId: params.referenceId,
    uploadUrl: params.uploadUrl,
  });
  let result = await newModule.save();
  return result ? true : false;
};

//TODO Get All Module
module.exports.getAllModule = async () => {
  const result = await Module.find({});
  return result;
};

//TODO Single Module
module.exports.getOneModule = async (params) => {
  const result = await Module.findById(params.moduleId);
  return result;
};

//TODO Update Module
module.exports.updateCourse = async (params) => {
  let updateModule = {
    name: params.name,
    description: params.description,
    deadlineDate: params.deadlineDate,
    typeOfStudy: params.typeOfStudy,
    referenceId: params.referenceId,
    uploadUrl: params.uploadUrl,
  };
  let result = await Module.findByIdAndUpdate(params.moduleId, updateModule);
  return result ? true : false;
};

//TODO Delete Module
module.exports.deleteModule = async (params) => {
  const result = await Module.findByIdAndDelete(params.moduleId);
  return result ? true : false;
};
