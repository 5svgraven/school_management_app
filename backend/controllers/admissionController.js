const Student = require("../models/Student");
const Admission = require("../models/Admission");

//start of Admission
//TODO Update Admission and update statut in student
module.exports.updateAdmission = async (params) => {
  let updateEnrollmentStatus = {
    status: params.studentStatus,
  };
  const resultStudentUpdate = await Student.findByIdAndUpdate(
    params.studentId,
    updateEnrollmentStatus
  );
  let resultAdmission = false;
  if (resultStudentUpdate) {
    let updateAdmission = {
      enrollmentStatus: params.admissionEnrollmentStatus,
    };
    resultadmission = await Admission.findByIdAndUpdate(
      admissionId,
      updateAdmission
    );
  }
  return resultAdmission ? true : false;
};
