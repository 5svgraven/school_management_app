const express = require("express");
const auth = require("../auth");
const router = express.Router();
const StudentController = require("../controllers/studentControllers");

//TODO Create Student
router.post("/", async (req, res) => {
  const result = await StudentController.createStudent(req.body);
  res.send(result);
});

//TODO Login
router.post("/login", async (req, res) => {
  const result = await StudentController.login(req.body);
  res.send(result);
});

//TODO Get all Student
router.get("/", auth.verify, async (res) => {
  const result = await StudentController.getAllStudent();
  res.send(result);
});

//TODO get details
router.get("/details", auth.verify, async (req, res) => {
  const student = auth.decode(req.headers.authorization);
  const result = await StudentController.detail({ studentId: student.id });
  res.send(result);
});

//TODO Get Single Student
router.get("/:studentId", auth.verify, async (req, res) => {
  let studentId = req.params.studentId;
  const result = await StudentController.getOneStudent({ studentId });
  res.send(result);
});

//TODO Update Student
router.put("/", auth.verify, async (req, res) => {
  const result = await StudentController.updateStudent(req.body);
  res.send(result);
});

//TODO Delete Student
router.delete("/", auth.verify, async (req, res) => {
  const result = await StudentController.deleteStudent(req.body);
  res.send(result);
});

module.exports = router;
