const express = require("express");
const auth = require("../auth");
const router = express.Router();
const CourseController = require("../controllers/courseController");

//TODO Create Course
router.post("/", auth.verify, async (req, res) => {
  const result = await CourseController.createCourse(req.body);
  res.send(result);
});

//TODO Get all Course
router.get("/", async (res) => {
  const result = await CourseController.getAllCourse();
  res.send(result);
});

//TODO Get Single Course
router.get("/:courseId", auth.verify, async (req, res) => {
  let courseId = req.params.courseId;
  const result = await CourseController.getOneCourse({ courseId });
  res.send(result);
});

//TODO Update Course
router.put("/", auth.verify, async (req, res) => {
  const result = await CourseController.updateCourse(req.body);
  res.send(result);
});

//TODO Delete Course
router.delete("/", auth.verify, async (req, res) => {
  const result = await CourseController.deleteCourse(req.body);
  res.send(result);
});

module.exports = router;
