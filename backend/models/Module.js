const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const moduleSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is required"],
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  referenceId: {
    type: String,
    require: [true, "Reference ID is required"],
  },
  uploadUrl: {
    type: String,
    require: [true, "upload link is required"],
  },
  submittedFilesByStudent: [
    {
      submittedUrl: {
        type: String,
      },
      studentId: {
        type: String,
      },
      submittedAt: {
        type: Date,
        default: new Date(),
      },
    },
  ],
  createdBy: {
    type: String,
    require: [true, "Created by is required"],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Module", moduleSchema);
