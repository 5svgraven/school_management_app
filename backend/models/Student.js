const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const studentSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: [true, "First name is required"],
  },
  lastname: {
    type: String,
    require: [true, "Last name is required"],
  },
  age: {
    type: String,
    require: [true, "Age is required"],
  },
  birthday: {
    type: Date,
    require: [true, "Date is required"],
  },
  gender: {
    type: String,
    require: [true, "GEnder is rquired"],
  },
  address: {
    type: String,
    require: [true, "Address Required"],
  },
  mobileNo: {
    type: String,
    require: [true, "Mobile Number is required"],
  },
  sectionId: {
    type: String,
    default: "",
  },
  courseId: {
    type: String,
    default: "",
  },
  year: {
    type: Number,
    default: 0,
  },
  role: {
    type: String,
    default: "Student",
  },
  imageUrl: {
    type: String,
    default: "",
  },
  parentsName: {
    type: String,
    default: "",
  },
  parentMobileNo: {
    type: String,
    default: "",
  },
  username: {
    type: String,
    required: [true, "Username is required"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  status: {
    type: String,
    default: "Not Enrolled",
  },
  school_history: [
    {
      schoolName: {
        type: String,
      },
      educationalAttainment: {
        type: String,
      },
      schoolYear: {
        type: String,
      },
    },
  ],
});

module.exports = mongoose.model("Student", studentSchema);
