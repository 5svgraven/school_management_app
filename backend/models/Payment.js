const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const paymentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is required"],
  },
  amount: {
    type: Number,
    require: [true, "Amount is required"],
  },
  payor: {
    type: String,
    require: [true, "Payor is required"],
  },
  remarks: {
    type: String,
    default: "",
  },
  status: {
    type: Boolean,
    default: false,
  },
  createdBy: {
    type: String,
    require: [true, "Created by is required"],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
  modifiedAt: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Payment", paymentSchema);
