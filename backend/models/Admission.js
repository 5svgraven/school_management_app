const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const admissionSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    default: new Date(),
  },
  studentId: {
    type: String,
    required: [true, "StudentID is required"],
  },
  paymentId: {
    type: String,
    require: [true, "Payment is required"],
  },
  enrollmentStatus: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("AdmissionSchema", admissionSchema);
