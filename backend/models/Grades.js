const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const gradeSchema = new mongoose.Schema({
  grade: {
    type: Number,
    required: [true, "Grade is required"],
  },
  typeOfStudy: {
    type: String,
    require: [true, "Type of Study is required"],
  },
  referenceId: {
    type: String,
    require: [true, "Reference ID is required"],
  },
  subjectId: {
    type: String,
    default: "",
  },
  studentId: {
    type: String,
    require: [true, "Teacher is required"],
  },
  createdBy: {
    type: String,
    require: [true, "Created by is required"],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Grade", gradeSchema);
