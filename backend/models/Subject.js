const mongoose = require("mongoose");
const Schema = require("mongoose");
const model = require("mongoose");

const subjectSchema = new mongoose.Schema({
  name: {
    type: String,
    require: [true, "Name is required"],
  },
  subjectType: {
    type: String,
    require: [true, "Subject Type is required"],
  },
  teacher: {
    type: String,
    require: [true, "Teacher is required"],
  },
  subjectStart: {
    type: String,
    require: [true, "Time Start is required"],
  },
  subjectEnd: {
    type: String,
    require: [true, "Time End is required"],
  },
  room: {
    type: String,
    require: [true, "Room is required"],
  },
  students: [
    {
      studentId: {
        type: String,
      },
    },
  ],
  modules: [
    {
      moduleId: {
        type: String,
      },
    },
  ],
  createdBy: {
    type: String,
    require: [true, "Created by is required"],
  },
  maxStudent: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Subject", subjectSchema);
